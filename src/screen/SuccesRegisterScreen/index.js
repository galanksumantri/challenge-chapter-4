import React, { useEffect } from 'react';
import { StatusBar, StyleSheet, Text, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { ButtonComponent, IconButton } from '../../component';
import { regSukses } from '../../redux';
import { colors, fonts } from '../../utils';

function SuccessRegisterScreen({ navigation }) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(regSukses(false));
  }, []);

  return (
    <View style={styles.page}>
      <StatusBar barStyle="dark-content" backgroundColor={colors.background.primary} />
      <View style={styles.container}>
        <Text style={styles.title}>Registration Completed</Text>
      </View>
      <View />
      <View style={styles.iconWrapper}>
        <IconButton type="Check" nonButton iconHeight={40} iconWidth={40} />
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.text}>We Sent Email Verification to</Text>
        <Text style={styles.text}>your email</Text>
      </View>
      <View style={styles.buttonWrapper}>
        <ButtonComponent
          title="Back to login"
          onPress={() => {
            navigation.navigate('LoginScreen');
          }}
        />
      </View>
    </View>
  );
}

export default SuccessRegisterScreen;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
    paddingHorizontal: 13,
    paddingVertical: 13,
  },
  container: {
    alignItems: 'center',
    marginTop: 100,
  },
  title: {
    fontSize: 20,
    color: colors.text.primary,
    fontFamily: fonts.primary[800],
  },
  text: {
    fontFamily: fonts.primary[800],
    color: colors.text.secondary,
    fontSize: 20,
    textAlign: 'center',
  },
  textWrapper: {
    marginTop: 250,
  },
  iconWrapper: {
    position: 'absolute', alignSelf: 'center', bottom: '50%',
  },
  buttonWrapper: {
    marginTop: 91,
  },
});
