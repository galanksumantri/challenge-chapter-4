import React, { useEffect } from 'react';
import { ScrollView, StatusBar, StyleSheet, Text, View, Dimensions, RefreshControl } from 'react-native';
import Share from 'react-native-share';
import { useDispatch, useSelector } from 'react-redux';
import { CardDetail, CardDetailSale, IconButton, Loading } from '../../component';
import { getDataBooksId, refresh } from '../../redux';
import { buatChannel, cancelAllLocalNotifications, colors, configure, fonts, kirimNotifikasi, showError } from '../../utils';

export default function BookDetailScreen({ route, navigation }) {
  const dispatch = useDispatch();
  const dataBooksId = useSelector((state) => state.dataBooks.booksId);
  const getToken = useSelector((state) => state.Auth.token);
  const isLoading = useSelector((state) => state.dataBooks.isLoading);
  const isRefreshing = useSelector((state) => state.dataBooks.isRefreshing);

  const { id } = route.params;

  useEffect(() => {
    dispatch(getDataBooksId(getToken, id));
  }, []);

  async function myCustomShare() {
    const shareOptions = {
      message: `Apakah kamu tertarik dengan buku ${dataBooksId.title} dengan rating ${dataBooksId.average_rating}/10 `,
    };

    try {
      await Share.open(shareOptions);
    } catch (error) {
      showError(error.message);
    }
  }

  const notification = () => {
    configure();
    buatChannel('1');
    cancelAllLocalNotifications();
    kirimNotifikasi('1', `Kamu menyukai ${dataBooksId.title}`, 'Terima Kasih');
  };

  const onRefresh = () => {
    dispatch(refresh(true));
    dispatch(getDataBooksId(getToken, id));
  };

  return (
    <View style={styles.page}>
      <StatusBar barStyle="dark-content" backgroundColor={colors.background.primary} />
      {
      isLoading ? <Loading /> : (
        <ScrollView refreshControl={(
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => onRefresh()}
          />
        )}
        >
          <View style={styles.header}>
            <View style={styles.iconButton}>
              <View style={{ flex: 3 }}>
                <IconButton type="Back" onPress={() => navigation.goBack()} />
              </View>
              <View
                style={{
                  flex: 2,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <View style={{ paddingLeft: 40 }}>
                  <IconButton type="Like" onPress={() => notification()} />
                </View>
                <View>
                  <IconButton type="Share" onPress={() => myCustomShare()} />
                </View>
              </View>
            </View>
            <View style={styles.cardDetailWrapper}>
              <CardDetail
                source={{ uri: dataBooksId.cover_image }}
                title={dataBooksId.title}
                author={dataBooksId.author}
                publisher={dataBooksId.publisher}
              />
            </View>
          </View>
          <View style={styles.cardDetailSaleWrapper}>
            <CardDetailSale
              rating={dataBooksId.average_rating}
              totalSale={dataBooksId.total_sale}
              harga={dataBooksId.price}
            />
          </View>
          <View style={styles.overviewWrapper}>
            <Text style={styles.label}>Overview</Text>
            <Text style={styles.overview}>
              {dataBooksId.synopsis}
            </Text>
          </View>
        </ScrollView>
      )
    }
    </View>
  );
}

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.background.primary,
  },
  iconButton: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  header: {
    elevation: 6,
    backgroundColor: colors.background.primary,
    paddingHorizontal: 13,
    paddingVertical: 13,
  },
  cardDetailSaleWrapper: {
    marginTop: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },

  label: {
    color: colors.text.primary,
    fontFamily: fonts.primary[700],
    fontSize: 18,
    marginTop: 10,
  },
  overviewWrapper: {
    margin: 13,
    width: windowWidth * 0.90,
    alignSelf: 'center',
    elevation: 6,
    borderRadius: 10,
    backgroundColor: colors.background.primary,
    padding: 13,
  },
  overview: {
    paddingTop: 7,
    fontFamily: fonts.primary[600],
    fontSize: 16,
    color: colors.text.primary,
    textAlign: 'justify',
  },
  cardDetailWrapper: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },

});
