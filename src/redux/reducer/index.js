import {
  GET_BOOKS_ID,
  GET_BOOKS_POPULAR,
  GET_BOOKS_RECOMMENDED,
  LOGIN,
  LOGOUT,
  REGISTER,
  REGISTER_SUCCESS,
  SET_LOADING,
  SET_ONLINE,
  SET_REFRESHING,
} from '../types';

const initialBookState = {
  booksRecommended: [],
  booksPopular: [],
  isLoading: false,
  isOnline: true,
  booksId: [],
  isRefreshing: false,
};

const initialAuthState = {
  token: null,
  isRegSukses: true,
};

export const dataReducers = (state = initialBookState, action) => {
  switch (action.type) {
    case GET_BOOKS_RECOMMENDED:
      return {
        ...state,
        booksRecommended: action.payload,
        isRefreshing: false,
      };
    case GET_BOOKS_POPULAR:
      return {
        ...state,
        booksPopular: action.payload,
        isLoading: false,
        isRefreshing: false,
      };
    case GET_BOOKS_ID:
      return {
        ...state,
        booksId: action.payload,
        isLoading: false,
        isRefreshing: false,
      };

    case SET_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    case SET_ONLINE:
      return {
        ...state,
        isOnline: action.payload,
      };

    case SET_REFRESHING:
      return {
        ...state,
        isRefreshing: action.payload,
      };

    default:
      return state;
  }
};

export const authReducers = (state = initialAuthState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        token: action.payload,
      };

    case LOGOUT:
      return {
        ...state,
        token: null,
      };

    case REGISTER:
      return {
        ...state,
        isRegSukses: true,
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        isRegSukses: action.payload,
      };

    default:
      return state;
  }
};
