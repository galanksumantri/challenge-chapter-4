import axios from 'axios';
import { GET_BOOKS_API, LOGIN_USER, REGISTER_USER } from '../../config';
import { showError, showSuccess } from '../../utils';
import {
  SET_ONLINE,
  SET_LOADING,
  SET_REFRESHING,
  LOGIN,
  REGISTER,
  REGISTER_SUCCESS,
  GET_BOOKS_ID,
  GET_BOOKS_POPULAR,
  GET_BOOKS_RECOMMENDED,
  LOGOUT,
} from '../types';

export const setLoading = (value) => ({
  type: SET_LOADING,
  payload: value,
});

export const register = () => ({
  type: REGISTER,
});

export const login = (data) => ({
  type: LOGIN,
  payload: data,
});

export const saveBookPopular = (data) => ({
  type: GET_BOOKS_POPULAR,
  payload: data,
});

export const saveBookRecommended = (data) => ({
  type: GET_BOOKS_RECOMMENDED,
  payload: data,
});

export const saveBookId = (data) => ({
  type: GET_BOOKS_ID,
  payload: data,
});

export const online = (value) => ({
  type: SET_ONLINE,
  payload: value,
});

export const refresh = (value) => ({
  type: SET_REFRESHING,
  payload: value,
});

export const regSukses = (value) => ({
  type: REGISTER_SUCCESS,
  payload: value,
});

export const logout = () => ({
  type: LOGOUT,
});

// LOGIN
export const loginUser = (email, password) => async (dispatch) => {
  try {
    await axios.post(LOGIN_USER, { email, password })
      .then((response) => {
        if (response.data.tokens.access.token) {
          dispatch(login(response.data.tokens.access.token));
          showSuccess('Login Sukses');
        }
      });
  } catch (error) {
    showError(error.message);
  }
};

// REGISTER
export const signupUser = (name, email, password) => async (dispatch) => {
  try {
    await axios.post(REGISTER_USER, { email, password, name })
      .then(() => {
        dispatch(register());
        showSuccess('Register Berhasil');
      });
  } catch (error) {
    dispatch(regSukses(false));
    showError(error.message);
  }
};

// GET_BOOKS_API_RECOMMENDED
export const getDataBooksRecommended = (token, limit) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    await axios.get(GET_BOOKS_API, { headers: { Authorization: `Bearer ${token}` }, params: { limit } })
      .then((response) => {
        dispatch(saveBookRecommended(response.data.results));
      });
  } catch (err) {
    showError(err.message);
    dispatch(refresh(false));
  }
};

// GET_BOOKS_API_POPULAR
export const getDataBooksPopular = (token) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    await axios.get(GET_BOOKS_API, { headers: { Authorization: `Bearer ${token}` } })
      .then((response) => {
        dispatch(saveBookPopular(response.data.results));
      });
  } catch (err) {
    showError(err.message);
    dispatch(refresh(false));
    dispatch(setLoading(false));
  }
};

// GET_BOOKS_API_BY_ID
export const getDataBooksId = (token, id) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    await axios.get(`${GET_BOOKS_API}/${id}`, { headers: { Authorization: `Bearer ${token}` } })
      .then((response) => {
        dispatch(saveBookId(response.data));
      });
  } catch (err) {
    showError(err.message);
    dispatch(refresh(false));
    dispatch(setLoading(false));
  }
};
