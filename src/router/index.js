import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import FlashMessage from 'react-native-flash-message';
import {
  BookDetailScreen, HomeTabScreen, LoginScreen, RegisterScreen, SplashScreen, SuccessRegisterScreen,
} from '../screen';

const Stack = createNativeStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator initialRouteName="SplashScreen">
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="BookDetailScreen"
        component={BookDetailScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="HomeTabScreen"
        component={HomeTabScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="RegisterScreen"
        component={RegisterScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SuccessRegisterScreen"
        component={SuccessRegisterScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

function Router() {
  return (
    <>
      <NavigationContainer>
        <MyStack />
      </NavigationContainer>
      <FlashMessage position="top" />
    </>

  );
}

export default Router;
