export const API_BASE_URL = 'http://code.aldipee.com/api/v1';
export const LOGIN_USER = 'http://code.aldipee.com/api/v1/auth/login';
export const REGISTER_USER = 'http://code.aldipee.com/api/v1/auth/register';
export const GET_BOOKS_API = 'http://code.aldipee.com/api/v1/books';
